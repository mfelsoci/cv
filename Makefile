all: cv-mfelsoci-fr

cv-mfelsoci-fr: cv-mfelsoci-fr.tex cv-fr biblio
	mkdir -p .aux
	latexmk --shell-escape -f -pdf -pdflatex -interaction=nonstopmode -auxdir=.aux $< > /dev/null 2> /dev/null

cv-fr: fr/activites-recherche.tex fr/activites-enseignement.tex fr/competences.tex

biblio: bibliography/personal.bib

clean:
	rm -f .aux/* *.pdf
