;; Nasledovné predstavuje „balíkospis“ zhodný s príkazovým riadkom, ktorý ste zadali.
;; Môžete si ho uložiť do súboru a potom ho použiť s ktorýmkoľvek „guix“ príkazom
;; prijímajúcim voľbu „--manifest“ (alebo „-m“).

(specifications->manifest
  (list "bash"
        "coreutils"
        "texlive"
        "texlive-biber"
        "make"))
