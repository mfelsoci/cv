# Curriculum Vitæ - Marek Felšöci

Ce répertoire contient le curriculum vitæ complet de
[Marek Felšöci](https://felsoci.sk) en français.

*This repository contains the complete curriculum vitæ of*
[Marek Felšöci](https://felsoci.sk) in English.

## Version française

Le point de départ de la version français du curriculum vitæ de Marek Felšöci
est le document « `cv-mfelsoci-fr.tex` ».

Pour construire le document PDF correspondant, tapez `make cv-mfelsoci-fr` dans
un terminal depuis la racine du dépôt. Notez que l'environnement logiciel
nécessaire à la construction est géré par [GNU Guix](https://guix.gnu.org) et
défini à l'aide des fichiers « `channels.scm` » et « `manifest.scm` » dans le
dossier « `.guix` » (cf. [Makefile](./Makefile)).

Pour construire et la version française et la version anglaise, tapez simplement
`make`.

## English version

The starting point of the English version of the curriculum vitæ of Marek
Felšöci is the document '`cv-mfelsoci-en.tex`'.

To build the corresponding PDF document, execute `make cv-mfelsoci-en` in a
terminal from the root of the repository. Note that the building software
environment is handled by [GNU Guix](https://guix.gnu.org) and defined through
the files '`channels.scm`' and '`manifest.scm`' located in the '`.guix`' folder
(see also [Makefile](./Makefile)).

To build both the English and the French version, execute simply `make`.