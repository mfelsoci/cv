\section{Activités de recherche}
\label{ar}

Actuellement, je mène mes activités de recherche dans le cadre de mon
post-doctorat auprès des équipes CAMUS\footref{fn:camus} (Inria Nancy Grand-Est)
et ICPS\footref{fn:icps} (ICube UMR 7357) où je travaille sur la parallélisation
automatique de code sources C/C++ par insertion de tâches (voir
\nameref{ar:post-doc}). Cependant, à ce jour, la majeur partie de mes activités
de recherche se sont déroulées dans le cadre de ma thèse de doctorat à
l'Université de Bordeaux auprès de l'équipe CONCACE\footref{fn:concace} (Inria
Bordeaux Sud-Ouest). Ma thèse portait sur le développement de méthodes pour
la résolution de grands systèmes linéaires couplés FEM/BEM creux/denses
découlant des problèmes aéroacoustiques (voir \nameref{ar:these}). Enfin, j'ai
effectué mes premiers pas vers la recherche en informatique lors de mes deux
stages, et plus particulièrement le stage de fin d'études, auprès des équipes
CAMUS et ICPS centrés sur une nouvelle structure de programmation, appelée XFOR,
et les outils logiciels associés (voir \nameref{ar:xfor}).

\subsection{Recherche post-doctorale (depuis 2023)}
\label{ar:post-doc}

Nous retrouvons ajourd'hui les processeurs multi-cœurs dans la plupart des
ordinateurs, des téléphones portables jusqu'aux grappes de calculs de haute
performance. Néanmoins, la conception et l'écriture d'applications parallèles
permettant un usage efficace de ces nombreuses ressources de calcul reste
souvent l'apanage des experts disposant des connaissances techniques et du temps
nécessaires pour optimiser leurs logiciels au mieux. De nombreux travaux de
recherche en compilation se sont intéressé à cette problématique et ont proposé
différentes approches de parallélisation automatique. Celles-ci sont
traditionnellement orientées vers la parallélisation de nids de boucles en se
basant sur le modèle polyèdrique.

Nous nous intéressons à la programmation parallèle par tâches. Dans ce modèle,
le programme est décomposé en tâches comportant une ou plusieurs instructions.
Aux tâches sont associés des contraintes de dépendance afin de permettre à un
moteur d'exécution d'ordonnancer et d'exécuter les tâches dans le bon ordre,
c'est-à-dire en respectant les dépendances éventuelles entre les tâches accèdant
aux mêmes données en mémoire. Ainsi, les tâches qui sont indépendantes l'une de
l'autre pourront être exécutées en parallèle. La difficulté dans l'optimisation
de cette approche est donc de trouver une décomposition correcte et efficace du
programme initial en tâches.

Dans ce cadre-là, nous travaillons sur un outil de parallélisation automatique
de code sources écrits en C ou en C++. Il s'agit d'un compilateur
source-à-source qui prend en entrée un code source séquentiel et qui le
transforme en un code source parallèle à base de tâches. Plus précisément, le
compilateur commence par lire le code source en entrée à partir duquel il génére
une représentation intermédiaire sous forme d'un graphe de tâches potentielles.
Dans un premier temps, toutes les instructions du code source de départ sont
considérées en tant que tâches potentielles et représentées par des nœuds du
graphe. Les dépendances éventuelles entre les tâches se traduisent par des
arêtes entre les nœuds correspondants du graphe. Il est ensuite possible
d'effectuer diverses analyses et transformations sur le graphe généré dont
l'objectif final est de décider quels nœuds deviendront effectivement des tâches
parallélisables. À partir de ce graphe, le compilateur est ensuite capable de
synthétiser un code source C ou C++ parallèle à base de tâches. L'utilisation
d'une représentation intermédiaire nous permet d'agir plus facilement sur le
code source à paralléliser ainsi que de nous abstraire de certaines contraintes
et particularités liées aux différentes technologies logicielles de
parallélisation par tâches que notre compilateur pourrait utiliser afin de
produire le code source parallèle. Pour le moment, le compilateur implémente
l'insertion des tâches à l'aide d'OpenMP.

Nous réalisons notre compilateur au sein de l'outil de transformation de code
source-à-source OptiTrust\footnote{\url{https://github.com/charguer/optitrust}}
auquel nous ajoutons les fonctionnalités nécessaires à l'analyse et à la
transformation du programme de départ. Dans ce contexte, nous collaborons
étroitement avec les auteurs et les contributeurs du projet OptiTrust, notamment
Arthur Chargéraud et Thomas Koehler, tous les deux nos collègues des équipes
CAMUS\footref{fn:camus} et ICPS\footref{fn:icps}.

Après avoir implémenté les fonctionnalités structurantes, telles que l'analyse
de dépendances, le graphe de tâches potentielles et la synthétisation du code
source parallèle, nous explorons actuellement différentes stratégies de
parallélisation.

\subsubsection{Animation de tutoriels}

\begin{itemize}
  \item \textbf{Towards a reproducible research study} \\
    \textsc{Tutoriel}, ComPAS 2023\footnote{Conférence francophone
    d'informatique en Parallélisme, Architecture et Système,
    \url{https://2023.compas-conference.fr/}\label{fn:compas23}}, Annecy,
    France, juillet 2023 \\
    \textit{Disponible en ligne :}
    \url{https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/tutorial/}
  \item \textbf{How to use Org mode and Guix to build a reproducible
    experimental study} \\
    \textsc{Tutoriel}, Workshop on Reproducible Software Environments for
    Research and High-Performance
    Computing\footnote{\url{https://hpc.guix.info/events/2023/workshop/}},
    Montpellier, France, novembre 2023
\end{itemize}

\subsubsection{Participation aux comités de programmes}

\begin{itemize}
  \item \textbf{Comité d'évaluation d'artefacts} \\
    \textsc{International Symposium on Code Generation and Optimization (CGO)
      2024} \\
    Edinburgh, Royame-Uni, mars 2024 \\
    \textit{Membres du comité :}
    \url{https://conf.researchr.org/committee/cgo-2024/cgo-2024-artifact-evaluation-artifact-evaluation-committee}
  \item \textbf{Comité de lecture} \\
    \textsc{Parallel Processing Letters} \\
    \textit{Site du journal :}
    \url{https://www.worldscientific.com/worldscinet/PPL}
  \item \textbf{Comité de reproductibilité} (en cours) \\
    \textsc{International Conference for High Performance Computing, Networking,
      Storage, and Analysis (SC) 2024} \\
    Atlanta, Géorgie, États-Unis, novembre 2024 \\
    \textit{Membres de tous les comités :}
    \url{https://sc24.supercomputing.org/planning-committee/}
\end{itemize}

\subsection{Thèse de doctorat (2019 - 2023)}
\label{ar:these}

Dans l'industrie aéronautique, l'aéroacoustique est utilisée pour modéliser la
propagation d'ondes sonores dans les flux d'air enveloppant un avion en vol. Il
est alors possible de simuler le bruit produit par un avion au niveau du sol
lors du décollage et de l'atterrissage afin d'assurer le respect des normes
environnementales et de permettre la conception de futurs modèles d'avion.
Contrairement à la plupart des autres simulations complexes en physique, la
méthode consiste en la résolution de systèmes linéaires couplés creux/denses.
Pour produire un résultat réaliste, le nombre d'inconnues dans le système peut
être extrêmement important ce qui fait de sa résolution un défi de taille. Ma
thèse de doctorat \cite{Thesis} était focalisée sur la conception et
l'évaluation d'algorithmes pour résoudre de grands systèmes linéaires de ce
genre.

D'un côté, nous avons proposé des algorithmes utilisant l'interface de
programmation (API) existante de solveurs directs creux et denses riches en
fonctionnalités et optimisés tels que MUMPS\footnote{solveur direct creux,
\url{https://mumps-solver.org/}}, HMAT\footnote{solveur direct dense,
\url{https://theses.hal.science/tel-01244260/}} et SPIDO\footnote{solveur direct
dense, développé en interne chez Airbus}. Grâce à ces algorithmes, nous arrivons
à contourner les défauts majeurs d'un usage basique de ces solveurs et profiter
pleinement de leurs fonctionnalités avancées telles que la compression
numérique, le calcul out-of-core et le parallélisme en mémoire distribuée. En
résumé, par rapport à une approche de référence de l'état de l'art, les
algorithmes proposés permettent de traiter des systèmes couplés creux/denses
jusqu'à 7 fois plus grands sur une seule machine multi-cœur à mémoire partagée
et plus de 6,5 fois plus grands dans un environnement à mémoire distribuée.

Dans le rapport de recherche \cite{RR20}, nous avons commencé par un travail de
formalisation et d'étude comparative des implémentations existantes chez Airbus.
Suite aux premiers nouveaux développements, j'ai présenté une étude préliminaire
des algorithmes proposés \cite{Compas21} lors de ComPAS 2021\footnote{Conférence
francophone d'informatique en Parallélisme, Architecture et Système, 2021,
\url{https://2021.compas-conference.fr/}}. Cette conférence de niveau nationale
au comité de lecture est idéalement adaptée aux doctorants souhaitant des
retours détaillés sur leurs travaux préliminaires. Absence d'actes de conférence
est volontaire et permet une soumission future des travaux dans un journal ou
une conférence internationale.

Ainsi, nous avons ensuite publié notre étude finale de tous les algorithmes
proposés en mémoire partagée dans \cite{IPDPS22} que j'ai présentée lors d'IPDPS
2022\footnote{International Parallel and Distributed Processing Symposium,
\url{https://www.ipdps.org/ipdps2022/2022-.html}}, une conférence de niveau
international. Il s'agit d'un des lieux majeurs du domaine dont la renommée est,
à juste titre, bien reconnue par les classements existants, et dans lesquels il
est important de publier.

Nous avons également conduit une étude multi-métrique des algorithmes proposés
incluant la consommation énergétique, l'utilisation de la mémoire ainsi que le
nombre d'opérations en virgule flottante \cite{SBACPAD22}. J'ai présenté cette
étude à l'occasion de SBAC-PAD 2022\footnote{International Symposium on Computer
Architecture and High Performance Computing,
\url{https://project.inria.fr/sbac2022/}}. Dans un premier temps, l'étude a
confirmé l'intérêt pour la compression numérique et le calcul out-of-core.
Ensuite, les profils de la consommation de puissance du processeur et de la
mémoire, ainsi que l'utilisation de la mémoire et le nombre d'opérations en
virgule flottante, nous ont permis de mieux comprendre le comportement de
l'application. Finalement, l'étude a révelé un goulot d'étranglement majeur dans
notre implémentation ainsi qu'un problème potentiel d'équilibrage de charge dans
le solveur direct creux.

Puis, nous avons brièvement présenté notre travail dans l'article court
\cite{Waves22} publié à la conférence Waves 2022\footnote{International
Conference on Mathematical and Numerical Aspects of Wave Propagation}. Enfin,
l'étude de ces algorithmes menée dans un environnement de mémoire distribuée et
présentée dans la thèse fait l'objet d'un rapport de recherche \cite{RR24}. Les
méthodes développées ont été implémentées et incluses dans les logiciels
propriétaires d'Airbus basés sur les solveurs MUMPS, SPIDO et HMAT.

D'un autre côté, nous avons évalué une API de solveur alternative qui s'appuie
sur un couplage de solveurs directs à base de tâches utilisant le même moteur
d'exécution. Une API personnalisée permet d'améliorer la composabilité et de
simplifier l'échange de données entre les solveurs pour une utilisation plus
efficace de ressources de calcul. Tandis que l'introduction de ces changements
substantiels dans des solveurs aux fonctionnalités avancés et maintenus par la
communauté ne peut se faire qu'à long terme à cause de la complexité de leur
code source (quelques centaines de milliers de ligne de code), nous avons pu
implémenter une preuve de concept de cette approche dans un prototype réduit.
Une étude expérimentale comparative préliminaire a validé notre implémentation
en confirmant qu'elle peut atteindre la précision de solution visée. En outre,
nous avons illustré les avantages potentiels d'une exécution asynchrone de
tâches et montré que même une preuve de concept de cette approche peut rivaliser
avec des méthodes précédemment proposées comme avec celles de l'état de l'art.

Ce travail a été le fruit d'une collaboration avec Alfredo Buttari (CNRS/IRIT --
équipe APO, Toulouse). En mai 2022, j'ai séjourné une semaine à Toulouse dans le
but de travailler sur l'incorporation de changements nécessaire dans le solveur
direct creux \texttt{qr\_mumps}\footnote{solveur creux direct à base de tâches,
\url{https://qr\_mumps.gitlab.io/}} utilisé et dont l'auteur est Alfredo
Buttari. J'ai eu l'occasion de soumettre un résumé \cite{Compas23} et de
présenter ce travail lors de ComPAS 2023\footnote{Conférence francophone
d'informatique en Parallélisme, Architecture et Système, 2023
\url{https://2023.compas-conference.fr/}}.

Outre la contribution principale, nous avons consacré un important effort à la
reproductibilité de notre travail. À cette fin, nous avons exploré les principes
de la programmation lettrée ainsi que les outils logiciels associés pour
garantir la reproductibilité des environnements expérimentaux et des expériences
numériques elles-mêmes sur différentes machines et sur des périodes de temps
étendues. La thèse elle-même contient un chapitre dédié à ce sujet. De plus, le
rapport technique \cite{RT20} publié en accompagnement du rapport de recherche
\cite{RR20} et décrivant l'envrionnement lettré et reproductible de l'étude
expérimentale de ce dernier représente notre premier travail abouti mettant en
œuvre les principes de reproductibilité étudiés. Par la suite, ces rapports
d'activités de la communauté \cite{Guix21, Guix22, Guix23} font l'état de nos
efforts continus. Dans ce cadre, j'ai également été amené à contribuer au
gestionnaire de paquets GNU~Guix (\url{https://guix.gnu.org}) en participant à
l'empaquetage de nouveaux logiciels ainsi qu'à la localisation de GNU~Guix et de
sa documentation en slovaque.

Les publications citées dans cette section, à l'exception de \cite{Waves22,
  Guix21, Guix22, Guix23}, ont été en majeure partie rédigées par mes soins.
Leur forme finale est le résultat des passes de relectures successives par les
co-auteurs suivies par des modifications de ma part.

\subsubsection{Publications}

Les auteurs sont classés suivant l'ordre alphabétique. La Table
\ref{publis:thesis} détaille les différentes publications.

\begin{table}[h!]
  \begin{center}
    \begin{tabular}{|c|c|c|c|}
      \hline
      \textbf{Publication} & \textbf{Format} & \textbf{Type} &
      \textbf{Comité de lecture} \\
      \hline
      \cite{RR24} & rapport de recherche & dépôt HAL & non \\
      \hline
      \cite{Guix23} & rapport technique & dépôt HAL & non \\
      \hline
      \cite{Guix22} & rapport technique & dépôt HAL & non \\
      \hline
      \cite{Thesis} & thèse de doctorat & dépôt HAL & jury de thèse \\
      \hline
      \cite{Compas23} & résumé & conférence nationale & non \\
      \hline
      \cite{SBACPAD22} & article & conférence internationale & oui \\
      \hline
      \cite{Guix21} & rapport technique & dépôt HAL & non \\
      \hline
      \cite{IPDPS22} & article & conférence internationale & oui \\
      \hline
      \cite{Waves22} & article court & conférence internationale & oui \\
      \hline
      \cite{RR20} & rapport de recherche & dépôt HAL & non \\
      \hline
      \cite{RT20} & rapport technique & dépôt HAL & non \\
      \hline
      \cite{Compas21} & article & conférence nationale & oui \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Détails des publications mentionnées dans cette section.}
  \label{publis:thesis}
\end{table}

\printbibliography[heading = none]

\subsubsection{Présentations}

\begin{itemize}
  \item \textbf{Study of the processor and memory power consumption of coupled
    sparse/dense solvers} \\
    \textsc{Conférence SBAC-PAD 2022}, Bordeaux, France, novembre 2022 \\
    \textit{Diaporama :}
    \url{https://thesis-mfelsoci.gitlabpages.inria.fr/slides/sbac-pad/sbac-pad.pdf}
  \item \textbf{Solution of coupled sparse/dense linear systems in an industrial
    aeroacoustic context} \\
    \textsc{Séminaire chez Airbus Central R\&T}, Issy-les-Moulineaux, France,
    septembre 2022 \\
    \textit{Diaporama :}
    \url{https://thesis-mfelsoci.gitlabpages.inria.fr/slides/airbus/airbus.pdf}
  \item \textbf{Solution of larger coupled sparse/dense linear systems in an
    industrial aeroacoustic context} \\
    \textsc{Séminaire de l'équipe CAMUS}, ICube UMR 7357,
    Illkirch-Graffenstaden, France, juin 2022 \\
    \textit{Diaporama :}
    \url{https://thesis-mfelsoci.gitlabpages.inria.fr/slides/camus/camus.pdf}
  \item \textbf{Direct solution of larger coupled sparse/dense FEM/BEM linear
    systems using low-rank compression} \\
    \textsc{Colloque Sparse Days
      2022}\footnote{\url{https://sparsedays.cerfacs.fr/fr/colloque-sparse-days-2022-a-saint-girons-iv/}},
    St-Girons, France, juin 2022 \\
    \textit{Diaporama :}
    \url{https://thesis-mfelsoci.gitlabpages.inria.fr/slides/sparse-days/sparse-days.pdf}
  \item \textbf{Direct solution of larger coupled sparse/dense linear systems
    using low-rank compression on single-node multi-core machines in an
    industrial context} \\
    \textsc{Conférence IPDPS 2022}, présence virtuelle, juin 2022 \\
    \textit{Diaporama :}
    \url{https://inria.hal.science/hal-03774145v1/file/presentation-ipdps2022.pdf}
  \item \textbf{Reconciling high-performance computing with the use of
    third-party libraries?} \\
    \textsc{Séminaire de l'équipe DATAMOVE (Inria)}, avec E. Agullo, présence
    virtuelle, mai 2022 \\
    \textit{Diaporama :}
    \url{https://thesis-mfelsoci.gitlabpages.inria.fr/slides/datamove/datamove.pdf}
  \item \textbf{An energy consumption study of coupled solvers for FEM/BEM
    linear systems: preliminary results} \\
    \textsc{Réunion pléniaire du projet ANR SOLHARIS\footnote{SOLvers for
      Heterogeneous Architectures over Runtime systems, Investigating
      Scalability, (ANR-19-CE46-0009)\label{fn:solharis}}}, Inria Bordeaux Sud-Ouest, France,
    février 2022 \\
    \textit{Diaporama :}
    \url{https://www.irit.fr/solharis/wp-content/uploads/2022/02/022022_marek_felsoci.pdf} \\
    \textit{Événement :}
    \url{https://www.irit.fr/solharis/solharis-plenary-meeting-09-10-02-2022/}
  \item \textbf{Towards memory-aware multi-solve two-stage solver for coupled
    FEM/BEM systems} \\
    \textsc{Réunion pléniaire du projet ANR SOLHARIS\footref{fn:solharis}},
    présence virtuelle, juillet 2021 \\
    \textit{Diaporama :}
    \url{https://www.irit.fr/solharis/wp-content/uploads/2021/07/072021_felsoci.pdf} \\
    \textit{Événement :}
    \url{https://www.irit.fr/solharis/solharis-plenary-meeting-02-07-2021/}
  \item \textbf{Guix et Org mode, deux amis du doctorant sur le chemin vers une
    thèse reproductible} \\
    \textsc{Atelier reproductibilité des environnements logiciels}, événement
    virtuel, mai 2021 \\
    \textit{Événement (diaporama et enregistrement vidéo disponibles) :} \\
    \url{https://hpc.guix.info/events/2021/atelier-reproductibilit%c3%a9-environnements/}
  \item \textbf{A preliminary comparative study of solvers for coupled FEM/BEM
    linear systems in a reproducible environment} \\
    \textsc{Réunion pléniaire du projet ANR SOLHARIS\footref{fn:solharis}},
    événement virtuel, décembre 2020 \\
    \textit{Diaporama :}
    \url{https://www.irit.fr/solharis/wp-content/uploads/2020/12/122020-Felsoci.pdf} \\
    \textit{Événement :}
    \url{https://www.irit.fr/solharis/solharis-plenary-meeting-07-08-12-2020/}
\end{itemize}

\subsubsection{Animation de tutoriels}

\begin{itemize}
  \item \textbf{Guix and Org mode, a powerful association for building a
    reproducible research study} \\
    \textsc{Tutoriel}, Inria Nancy Grand-Est, France, juin 2022 \\
    \textit{Disponible en ligne :}
    \url{https://tuto-techno-guix-hpc.gitlabpages.inria.fr/guidelines/}
\end{itemize}

\subsection{Stage de fin d'études (2019)}
\label{ar:xfor}

Le travail que j'ai effectué lors de mon stage de fin d'études s'inscrit dans le
domaine de l'optimisation de programmes et en particulier de boucles FOR en
utilisant le modèle polyèdrique. Plus particulièrement, j'ai travaillé sur la
structure de programmation XFOR\footnote{Xfor: Semantics and Performance --
\url{https://hal-lara.archives-ouvertes.fr/hal-02475775/}}. Sa syntaxe est très
similaire à celle des boucles FOR standards du langage C. Cependant, elle permet
de regrouper et de gérer plusieurs boucles FOR en même temps. Grâce à ses deux
paramètres spécifiques, que sont le \textit{grain} et l'\textit{offset}, le
programmeur peut influer de manière plus simple et plus intuitive sur la façon
dont ces boucles doivent s'exécuter.

L'objectif est d'ajuster l'ordre d'exécution des instructions à l'intérieur des
boucles gérées, de manière à mettre en valeur les possibilités qu'offrent les
architectures d'ordinateur modernes. Il s'agit principalement d'optimiser
l'utilisation de la mémoire cache et d'exploiter au mieux les capacités de
vectorisation. Un programme re-écrit en utilisant XFOR peut devenir jusqu'à 6
fois plus rapide par rapport à sa version originale.

Un des principaux outils dédiés à cette structure est le compilateur «~Iterate,
But Better!~» (IBB) permettant de traduire des boucles XFOR en boucles FOR
équivalentes. Ainsi, le programme traduit peut être compilé par un compilateur
quelconque du langage C. Cette nécessité de compiler un programme XFOR deux fois
a en partie motivé l'intégration de la structure XFOR dans un compilateur de
production tel que Clang/LLVM. Celle-ci permettrait la compilation directe de
programmes XFOR et pourrait aider à promouvoir la structure au sein de la
communauté de programmeurs.

Dans le cadre de ce stage, j'ai été amené à étendre l'analyseur lexical et
l'analyseur syntaxique du compilateur Clang/LLVM, pour que ce dernier soit en
mesure de reconnaître et de correctement traduire des boucles XFOR. Aussi, j'ai
implémenté la transformation de celles-ci en représentation intermédiaire
utilisée par le compilateur pour produire des fichiers exécutables. À l'issue du
stage, le Clang/LLVM étendu (\url{https://gitlab.inria.fr/xfor/xfor-clang})
était capable de compiler des programmes aux boucles XFOR qu'elles soient
simples ou imbriquées.

En plus de favoriser la vectorisation par le processeur, la structure XFOR
permet parfois de mettre en évidence des possibilités de parallélisation à un
grain moins fin. Dans une seconde partie du stage, j'ai donc exploré
l'utilisation de la bibliothèque de parallélisation OpenMP dans les nids de
boucles XFOR.

Le mémoire de mon stage de fin d'études est disponible en ligne à l'adresse
\url{https://felsoci.pages.unistra.fr/MastersThesis/}.

\subsubsection{Présentations}

\begin{itemize}
  \item \textbf{XFOR loops, integration into the Clang/LLVM compiler and
    extenstion to parallel programming} \\
    \textsc{Software corner}, ICube UMR 7357, Illkirch-Graffenstaden, France,
    juin 2019 \\
    \textit{Diaporama :} \url{https://felsoci.sk/others/software-corner.pdf}
  \item \textbf{On the XFOR programming structure} \\
    \textsc{Introduction à la recherche pour les étudiants de Licence}, U.F.R.
    de mathématique et d'informatique, Université de Strasbourg, France, avril
    2019
\end{itemize}
